﻿using System;
namespace Bank
{
    class Account
    {
        protected string firstName;
        protected string lastName;
        protected double amount;
        public Account(string newfirstName, string newlastName, double newamount)
        {
            firstName = newfirstName;
            lastName = newlastName;
            amount = newamount;
        }

        public double getAmount         
        {
            get { return amount; }                    
        }
        public string getfirstName
        {
            get { return firstName; }
        }
        public string getLastName
        {
            get { return lastName; }
        }
        public double withdraw(double newamount)
        {
            if (amount < newamount)
            {
                Console.WriteLine("Error: Insufficient funds, transaction cancelled");
                return amount;
            }
            amount = amount - newamount;
            
            return amount;
        }
        public double deposit(double newamount)
        {
            if (newamount < 0)
            {
                Console.WriteLine("cannot deposit negative amount, transaction cancelled");
                return amount;
            }
            amount = amount + newamount;
            return amount;
        }

        public bool transfer(Account givingAccount, Account receivingAccount, double amountTransfer)
        {
            double givingStartingValue = givingAccount.getAmount;
            double receivingStartingValue = receivingAccount.getAmount;

            givingAccount.withdraw(amountTransfer);
            if (givingAccount.getAmount == givingStartingValue || amountTransfer < 0)
            {
                Console.WriteLine("an error has occured during the transaction, insufficient funds.");
                return false;
            }
            receivingAccount.deposit(amountTransfer);
            if (receivingAccount.getAmount == receivingStartingValue || amountTransfer < 0)
            {
                Console.WriteLine("an error has occured during the transaction, insufficient funds.");
                return false;
            }
            Console.WriteLine("Transfer Successful");
            Console.Write(givingAccount.getfirstName + " ");
            Console.Write(givingAccount.getLastName);
            Console.Write(" new Amount: ");
            Console.Write(givingAccount.getAmount);

            Console.WriteLine();
            Console.Write(receivingAccount.getfirstName + " ");
            Console.Write(receivingAccount.getLastName);
            Console.Write(" new Amount: ");
            Console.Write(receivingAccount.getAmount);

            return true;
        }

    }


    class Checking : Account
    {
        public Checking(string newfirstName, string newlastName, double newamount) : base(newfirstName, newlastName, newamount)
        {            
        }
    }
    class Savings : Account
    {
        public Savings(string newfirstName, string newlastName, double newamount) : base(newfirstName, newlastName, newamount)
        {
        }
    }
    class test
    {
        static void Main()
        {
            Checking testAccount = new Checking("Brad", "Stephen", 1000);
            Account testAccount2 = new Account("Miriam", "Stephen", 1000);
            Savings testAccount3 = new Savings("Bernard", "Stephen", 1000);

            testAccount2.transfer(testAccount, testAccount3, 300);
            Console.WriteLine();
        }
    }

}